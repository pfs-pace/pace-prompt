# PACE Prompt

## A fancy **.bashrc** file for Linux users

## What is this about?

For years I've been using this file as my default **.bashrc**. The first thing I do on every Linux machine I'm working on is to slap this file into my home directory. So I though some people could also benefit from it. Also this is my first **CC0** release. \o/

## What does it do?

This is a custom **.bashrc** that comes plenty of aliases and keyboard shortcuts to make life easier. It displays key information split in three lines.

- The first line displays your working directory, the number of files directory total size
- The second line displays the **username@host** followed by uptime and a timestamp so you can easily keep track when your entered a given command or how long it took to complete
- The third line is the command prompt itself

## Disclaimer

If you work on a high performance environment with tons of thousands of files, this **.bashrc** is not for you. It will badly damage your terminal's performance.

## Available colors

Blue

![Blue Prompt](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/blinking_blue_prompt.gif)

Yellow

![Yellow Prompt](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/blinking_yellow_prompt.gif)

Green

![Green Prompt](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/blinking_green_prompt.gif)

Purple

![Purple Prompt](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/blinking_purple_prompt.gif)

Grey

![Grey Prompt](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/blinking_grey_prompt.gif)

Red

![Red Prompt](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/blinking_red_prompt.gif)

## Custom Aliases

**l**
same as

`ls --color=auto -h`

**dir**
same as

`dir --color=auto`

**vdir**
same as

`vdir --color=auto`

**grep** 
same as 

`grep --color=auto`

**fgrep**
same as

`fgrep --color=auto`

**egrep**
same as

`egrep --color=auto`


**rm** same as 

`rm -i`

asks for confirmation before removing files/directories

**cp** same as

`cp -i`

asks for confirmation before overwriting files

**mv** same as

`mv -i`

asks for confirmation before moving/renaming files and directories

**cls** same as

`clear`

**ll** same as

`ls -l --color=auto -h`


**la** same as

`ls -a --color=auto -h`

**lla** same as

`ls -la --color=auto -h`

**lsat** same as

`lsattr`

**llat** same as

`lsattr -l`

**df** same as

`df -H`

**ldir** same as

`ls -d */ -h`

Simple listing (directories only)

**lldir** same as

`ls -ld */ -h`

Detailed listing (directories only)

**ldira** same as

`ls -d */ .*/ -h`

Simple listing including hidden directories

**lldira**

`ls -ld */ .*/ -h`

Detailed listing including hidden directories

**mce** same as

`mcedit`

Midnight Commander Text Editor

**getvideo** same as

`yt-dlp -f 'bestvideo[height<=720]+bestaudio/best[height<=720]' $1 $2 $3`

Gets video from a given url via **yt-dlp** 

(url must be between quotes)

(change the video resolution as you wish. 720 is set for HD)

**getaudio** same as

`yt-dlp -x --audio-format 'mp3' --yes-playlist $1 $2 $3`

gets audio from a given url via **yt-dlp**

(url must be between quotes)

**lx** same as

`lynx --display_charset=utf-8`

(lynx terminal web browser)

## Gallium HUD

Gallium is a powerful Heads Up Display provided by MESA opensource drivers able to monitor your GPU and CPU resources.

To enable Gallium HUD you must set it's config from the command line and trigger a signal to toggle it's on screen status.

The alias **gallium** is the same as

`GALLIUM_HUD="cpu,GPU-load:100,.dfps:60,draw-calls,.drequested-VRAM,frametime,temperature" GALLIUM_HUD_VISIBLE=false GALLIUM_HUD_PERIOD=0.5 GALLIUM_HUD_SCALE=1 GALLIUM_HUD_TOGGLE_SIGNAL=10 nohup $GALLIUM_COMMAND &`

type **gallium** before your executable file. Eg.:

`$ gallium ./my_game`

This alias will set up the variables for gallium and create a hidden file named **.gallium_command** inside your home directory. This file stores the last executable name which will be used to toggle the HUD on / off via keyboard shortcut.

![This is how the HUD will be displayed on the screen after you enable the HUD via signal -10 or by pressing CTRL + G](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/tito_gallium.png)


## Switching the prompt colors

Just uncomment the line for the color you wish and comment the previous one. 
Make sure you do it for ordinary and root users.

![Switching colors](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/prompt_nano.png)

## Keyboard shortcuts

To enable GALLIUM HUD on screen after launching an executable via terminal press in any terminal window:

**CTRL + G**

To "climb up" directory instead of typing the unfamous `cd ..` multiple times just press:

**CTRL + H** or **CTRL + BACKSPACE**

## How to install it

Make a backup of your **.bashrc** file. Then download and rename the file **PACE_prompt** to **.bashrc** and move it to your **home directory** for ordinary user or **/root** for your root user.

## Advices

Don't forget to install NeoFetch, YT-DLP and Midnight Commander

![PACE Prompt and Neofetch](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/preview/prompt_neofetch.png)

## Authors and acknowledgment
by Paulo Freire Silva

[HubZilla](https://zotum.net/channel/pfsilva)

## License
**CC0 - Public Domain**

Do as you like. 

## Support

If you wish to buy me a coffee with **Monero**, here is my wallet:

![837KTqwPhTvJT7saf3EyMf1pEduq6fwAEVcHbE4Cv2ym9WHPPhhgGXrD7RM3qhLt8defRga3fhzhqd16QvmPwaaz7FhEaKW](https://gitlab.com/pfs-pace/pace-prompt/-/raw/main/PFS_PACE_-_Monero.png)

Please confirm the wallet address:
**837KTqwPhTvJT7saf3EyMf1pEduq6fwAEVcHbE4Cv2ym9WHPPhhgGXrD7RM3qhLt8defRga3fhzhqd16QvmPwaaz7FhEaKW**

Thank you and have fun! :-)

